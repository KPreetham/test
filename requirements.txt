requests==2.18.4
flask==1.0.2
django==2.1.1
bcrypt==3.1.4
ipython
gunicorn==19.9.0
psycopg2==2.7.5
