sudo apt update;
sudo apt upgrade


sudo apt-get install build-essential libssl-dev libffi-dev python-dev python3-dev --assume-yes
sudo apt-get install nginx --assume-yes
sudo apt-get install openssh-server --assume-yes
sudo apt-get install postgresql libpq-dev postgresql-client postgresql-client-common --assume-yes
sudo apt-get install python-pip python3-pip --assume-yes
sudo apt-get install software-properties-common
sudo apt-get install supervisor --assume-yes
sudo apt-get install vim --assume-yes

