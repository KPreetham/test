import logging
import os
import sys

import django
from flask import Flask
from flask import request
from flask.views import MethodView

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
flask_app = Flask(__name__)


def django_setup():
    script_directory = os.path.dirname(os.path.realpath(__file__))
    sys.path.insert(0, "{script_directory}/tracer".format(script_directory=script_directory))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tracer.settings")
    django.setup()


django_setup()

from tracker.models import AccidentList, SensorValues, AlertList


@flask_app.route("/ping")
def ping():
    return "PONG, PONG"


class SensorData(MethodView):
    methods = ["POST", "GET"]

    def __init__(self):
        super().__init__()
        self.__post_data = request.json

    def process(self):
        data = self.__post_data
        SensorValues.objects.create(xValue=data["xValue"], yValue=data['yValue'], zValue=data['zValue'],
                                    xDev=data['xDev'], yDev=data['yDev'], zDev=data['zDev'],
                                    threshold=data['threshold'])

    def get(self):
        logger.error("get method")
        return "OK GET"

    def post(self):
        self.process()
        return "ok"


class AccidentApi(MethodView):
    methods = ["POST", "GET"]

    def __init__(self):
        super().__init__()
        self.__post_data = request.json

    def process(self):
        data = self.__post_data

        AccidentList.objects.create(lat=float(data['lat']), lon=float(data['lon']), message=data["message"])

        pass

    def get(self):
        logger.error("get method")
        return "OK GET"

    def post(self):
        self.process()
        return "ok"


class AlertApi(MethodView):
    methods = ["POST", "GET"]

    def __init__(self):
        super().__init__()
        self.__post_data = request.json

    def post(self):
        self.process()
        return "OK"

    def process(self):
        data = self.__post_data

        AlertList.objects.create(message=data['message'])


def add_url(url, view_class, view_name, version="v1"):
    flask_app.add_url_rule(rule="/api/{version}/{url}".format(version=version, url=url),
                           view_func=view_class.as_view(view_name))


def register_views():
    add_url("update-sensor", SensorData, "sensor")
    add_url("accident-data", AccidentApi, "accident")
    add_url("alert-data", AlertApi, "alert")


register_views()

if __name__ == '__main__':
    flask_app.run(debug=True, host="0.0.0.0", port=5000)
    # flask_app.run(debug=True)
