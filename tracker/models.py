from django.db import models

from django.utils import timezone
import pytz

timezone.activate(pytz.timezone("Asia/Kolkata"))


# Create your models here.
class SensorValues(models.Model):
    xValue = models.FloatField()
    yValue = models.FloatField()
    zValue = models.FloatField()

    xDev = models.FloatField()
    yDev = models.FloatField()
    zDev = models.FloatField()

    threshold = models.FloatField()

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "sensor"


class AccidentList(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    lat = models.FloatField()
    lon = models.FloatField()
    message = models.TextField()

    class Meta:
        db_table = "accident"


class AlertList(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    message = models.TextField()

    class Meta:
        db_table = "alert"
