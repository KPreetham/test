import requests

import random

url = "http://127.0.0.1:5000/api/v1/update-sensor"
url2 = "http://127.0.0.1:5000/api/v1/accident-data"


def get():
    data = {
        "xValue": random.choice(range(15)),
        "yValue": random.choice(range(15)),
        "zValue": random.choice(range(15)),
        "xDev": random.choice(range(15)),
        "yDev": random.choice(range(15)),
        "zDev": random.choice(range(15)),
        'threshold': 2.3
    }
    return data


accident_data = {
    "lat": 12.00,
    "lon": 72.00
}

for i in range(100):
    r = requests.post(url, json=get())
    print(r.text)

# r = requests.post(url2, json=accident_data)
# print(r.text)
#
