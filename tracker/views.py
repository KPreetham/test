from django.shortcuts import render

from django.shortcuts import render
from django.http import HttpResponse

from tracker.models import SensorValues

import datetime

a = datetime.timedelta(hours=5, minutes=30)


def chart(request):
    x = []
    y = []
    z = []

    all_data = list(SensorValues.objects.all().order_by('id'))[-2000:]
    # all_data = all_data[::-1]
    # all_data = all_data[:2000]
    # all_data = all_data[:2000]

    for data in all_data:
        x.append([data.created + a, data.xValue])
        y.append([data.created + a, data.yValue])
        z.append([data.created + a, data.zValue])

    return render(request, 'index.html', {'x': x, 'y': y, 'z': z})
