from django.contrib import admin

# Register your models here.
from .models import SensorValues, AccidentList, AlertList


@admin.register(SensorValues)
class SensorValueAdmin(admin.ModelAdmin):
    list_display = ("created", "xValue", "yValue", "zValue", "xDev", "yDev", "zDev", "threshold")

    readonly_fields = list_display


@admin.register(AccidentList)
class AccidentListAdmin(admin.ModelAdmin):
    list_display = ("created", "lat", "lon", "message")
    readonly_fields = list_display


@admin.register(AlertList)
class AlertListAdmin(admin.ModelAdmin):
    list_display = ("created", "message")
    readonly_fields = list_display
